function [L,U] = Gauss_Elim(A)
%Uses gaussian elimination to calculate the LU decompisition of the input
%matrix A. 

%Get matrix dimentions
[m n] =  size(A);

%Determine if matrix is square. If it isn't produce error.
if m ~= n
    error('Matrix is not square.')
end

%Initialize U to be A, L to be idenity matrix
U = A; L = eye(m);

%Preform Guassian Elimination. Will start by constructing L from current U
%then recalculating U. 
for k = 1:m-1
    for j = k+1:m
        L(j,k) = U(j,k)/U(k,k);
        U(j,k:m) = U(j,k:m)-L(j,k)*U(k,k:m);
    end
end
