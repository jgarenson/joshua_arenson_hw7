function [L,U] = GE_PartialPivot(A)
%Preforms Gaussian elimination with partial pivioting in order to calculate
%LU decompisition of A. This method is more stable than non-piviot methods.

[m,n]=size(A);

%Tests to make sure matrix is square. 
if m ~= n
    error('Matrix is not square.')
end

%Inititalize
U = A; L = eye(m); P = eye(m);

for k = 1:m-1
    %reset P
    P = eye(m);
    %find two rows to swap such that i ge k and the row swapped is max
    %valued
    [M,I] = max(abs(U(k:end,k)));
    %get right row
    i = I+k;
    %swap
    U = P*U;
    L = P*L;
    %calculate L_k
    for j = k+1:m
        L(j,k) = U(j,k)/U(k,k);
        U(j,k:m) = U(j,k:m)-L(j,k)*U(k,k:m);
    end
end
